#!/usr/bin/env python
# coding: utf-8

# In[4]:


#soal1
#Make a class called University. 
#The __init__() method for University should store two attributes: a university_name and a school_type. 
#Make a method called describe_university() that prints these two pieces of information, 
#and a method called open_university() that prints a message indicating that the university open time in a week. 
#Make an instance called university from your class. Print the two attributes individually, and then call both methods. 
#Then, three different instances from the class, and call describe_university() for each instance.
class university:
    def __init__(self, university_name, school_type):
        self.un = university_name
        self.st = school_type
    def describe_university(self):
        print('University Name : {}'.format(self.un))
        print('School Type : {}'.format(self.st))
    def open_university(self):
        print('The University is opened at 9 a.m - 5 p.m from Monday to Friday')
univ = university('Universitas Prasetiya Mulya','School Of Bussiness Economic and Science Technology Engineering Mathematics')
print(univ.describe_university())   


# In[5]:


#soal1
#Make a class called University. 
#The __init__() method for University should store two attributes: a university_name and a school_type. 
#Make a method called describe_university() that prints these two pieces of information, 
#and a method called open_university() that prints a message indicating that the university open time in a week. 
#Make an instance called university from your class. Print the two attributes individually, and then call both methods. 
#Then, three different instances from the class, and call describe_university() for each instance.
class university:
    def __init__(self, university_name, school_type):
        self.un = university_name
        self.st = school_type
    def describe_university(self):
        print('University Name : {}'.format(self.un))
        print('School Type : {}'.format(self.st))
    def open_university(self):
        print('The University is opened at 9 a.m - 5 p.m from Monday to Friday')
univ = university('Universitas Prasetiya Mulya','School Of Bussiness Economic and Science Technology Engineering Mathematics')
print(univ.open_university())


# In[7]:


#soal1
#Make a class called University. 
#The __init__() method for University should store two attributes: a university_name and a school_type. 
#Make a method called describe_university() that prints these two pieces of information, 
#and a method called open_university() that prints a message indicating that the university open time in a week. 
#Make an instance called university from your class. Print the two attributes individually, and then call both methods. 
#Then, three different instances from the class, and call describe_university() for each instance.
class university:
    def __init__(self, university_name, school_type):
        self.un = university_name
        self.st = school_type
    def describe_university(self):
        print('University Name : {}'.format(self.un))
        print('School Type : {}'.format(self.st))
    def open_university(self):
        print('The University is opened at 9 a.m - 5 p.m from Monday to Friday')
univ = university('Universitas Prasetiya Mulya','School Of Bussiness Economic and Science Technology Engineering Mathematics')
print(univ.describe_university())
print(univ.open_university())


# In[9]:


#soal1
#Make a class called University. 
#The __init__() method for University should store two attributes: a university_name and a school_type. 
#Make a method called describe_university() that prints these two pieces of information, 
#and a method called open_university() that prints a message indicating that the university open time in a week. 
#Make an instance called university from your class. Print the two attributes individually, and then call both methods. 
#Then, three different instances from the class, and call describe_university() for each instance.
class university:
    def __init__(self, university_name, school_type):
        self.un = university_name
        self.st = school_type
    def describe_university(self):
        print('University Name : {}'.format(self.un))
        print('School Type : {}'.format(self.st))
    def open_university(self):
        print('The University is opened at 9 a.m - 5 p.m from Monday to Friday')
univ = university('Universitas Prasetiya Mulya','School Of Bussiness Economic and Science Technology Engineering Mathematics')
print(univ)


# In[10]:


#soal1
#Make a class called University. 
#The __init__() method for University should store two attributes: a university_name and a school_type. 
#Make a method called describe_university() that prints these two pieces of information, 
#and a method called open_university() that prints a message indicating that the university open time in a week. 
#Make an instance called university from your class. Print the two attributes individually, and then call both methods. 
#Then, three different instances from the class, and call describe_university() for each instance.
class university:
    def __init__(self, university_name, school_type):
        self.un = university_name
        self.st = school_type
    def describe_university(self):
        print('University Name : {}'.format(self.un))
        print('School Type : {}'.format(self.st))
    def open_university(self):
        print('The University is opened at 9 a.m - 5 p.m from Monday to Friday')
univ = university('Universitas Prasetiya Mulya','School Of Bussiness Economic and Science Technology Engineering Mathematics')
print(univ)
print(univ.describe_university())
print(univ.open_university())

