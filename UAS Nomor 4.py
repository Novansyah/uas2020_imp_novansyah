#!/usr/bin/env python
# coding: utf-8

# In[2]:


#create plot of Open Price
import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
tesla1 = pd.read_csv('Tesla_Stock.csv', index_col='Date', parse_dates = True)
ford1 = pd.read_csv('Ford_Stock.csv', index_col='Date', parse_dates = True)
gm1 = pd.read_csv('GM_Stock.csv', index_col = 'Date', parse_dates = True)
tesla1['Open'].plot(label = 'Tesla', title = 'Open Price')
ford1['Open'].plot(label = 'Ford')
gm1['Open'].plot(label = 'GM')
plt.legend()

