#!/usr/bin/env python
# coding: utf-8

# In[36]:


#create plot of Volume
import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
tesla1 = pd.read_csv('Tesla_Stock.csv', index_col='Date', parse_dates = True)
ford1 = pd.read_csv('Ford_Stock.csv', index_col='Date', parse_dates = True)
gm1 = pd.read_csv('GM_Stock.csv', index_col = 'Date', parse_dates = True)
tesla1['Volume'].plot(label = 'Tesla',title = 'Volume Traded',xlim=['2012-01-01','2017-01-01'])
ford1['Volume'].plot(label = 'Ford',title = 'Volume Traded',xlim=['2012-01-01','2017-01-01'])
gm1['Volume'].plot(label = 'GM',title = 'Volume Traded',xlim=['2012-01-01','2017-01-01'])
plt.legend()


# In[28]:


ford = pd.read_csv('Ford_Stock.csv')
ford


# In[32]:


max(ford1['Volume'])


# In[34]:


ford2 = ford[ford['Volume']=='220362796']
ford3 = ford2.groupby('Date')
ford3.count()


# In[ ]:


#Maximum volume jatuh tanggal 18 Desember 2013

