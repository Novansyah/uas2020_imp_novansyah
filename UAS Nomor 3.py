#!/usr/bin/env python
# coding: utf-8

# In[1]:


#We'll be analyzing stock data related to a few car companies, from Jan 1 2012 to Jan 1 2017. Use file Tesla_Stock.csv. 


# In[2]:


import numpy as np
import pandas as pd


# In[3]:


tesla = pd.read_csv('Tesla_Stock.csv')
tesla


# In[4]:


ford = pd.read_csv('Ford_Stock.csv')
ford


# In[5]:


gm = pd.read_csv('GM_Stock.csv')
gm

