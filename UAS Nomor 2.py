#!/usr/bin/env python
# coding: utf-8

# In[2]:


#Make a class called Student. Create two attributes called first_name and last_name, 
#and then create several other attributes that are typically stored in a student profile. 
#Make a method called describe_student() that prints a summary of the student’s information.
#Make another method called greet_student() that prints a personalized greeting to the user.
class Student():
    def __init__(self, first_name, last_name, ID_student, major, year):
        self.fn = first_name
        self.ln = last_name
        self.id = ID_student
        self.m  = major
        self.y  = year
    def describe_student(self):
        print('First Name : {}'.format(self.fn))
        print('Second Name : {}'.format(self.ln))
        print('Student ID : {}'.format(self.id))
        print('Student Major : {}'.format(self.m))
        print('Student Year : {}'.format(self.y))
    def greet_student(self):
        print('Hello {}, Have A Great Day for Everyday. Enjoy Your Study in Major {}'.format(self.fn, self.m))
stu = Student('Novan','Syah','23501910004','Software Engineering','2019')
print(stu.describe_student())


# In[3]:


#Make a class called Student. Create two attributes called first_name and last_name, 
#and then create several other attributes that are typically stored in a student profile. 
#Make a method called describe_student() that prints a summary of the student’s information.
#Make another method called greet_student() that prints a personalized greeting to the user.
class Student():
    def __init__(self, first_name, last_name, ID_student, major, year):
        self.fn = first_name
        self.ln = last_name
        self.id = ID_student
        self.m  = major
        self.y  = year
    def describe_student(self):
        print('First Name : {}'.format(self.fn))
        print('Second Name : {}'.format(self.ln))
        print('Student ID : {}'.format(self.id))
        print('Student Major : {}'.format(self.m))
        print('Student Year : {}'.format(self.y))
    def greet_student(self):
        print('Hello {}, Have A Great Day for Everyday. Enjoy Your Study in Major {}'.format(self.fn, self.m))
stu = Student('Novan','Syah','23501910004','Software Engineering','2019')
print(stu.greet_student())


# In[4]:


#Make a class called Student. Create two attributes called first_name and last_name, 
#and then create several other attributes that are typically stored in a student profile. 
#Make a method called describe_student() that prints a summary of the student’s information.
#Make another method called greet_student() that prints a personalized greeting to the user.
class Student():
    def __init__(self, first_name, last_name, ID_student, major, year):
        self.fn = first_name
        self.ln = last_name
        self.id = ID_student
        self.m  = major
        self.y  = year
    def describe_student(self):
        print('First Name : {}'.format(self.fn))
        print('Second Name : {}'.format(self.ln))
        print('Student ID : {}'.format(self.id))
        print('Student Major : {}'.format(self.m))
        print('Student Year : {}'.format(self.y))
    def greet_student(self):
        print('Hello {}, Have A Great Day for Everyday. Enjoy Your Study in Major {}'.format(self.fn, self.m))
stu = Student('Novan','Syah','23501910004','Software Engineering','2019')
print(stu.describe_student())
print(stu.greet_student())

